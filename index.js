const { utils, libs, controllers, config, validator, results, services, routes, logger } = require('../util');

const middlewaresList = [
  utils.framework.createMiddleware(services.middleware.basicMiddleware(services.server.create)),
  utils.framework.createMiddleware(services.middleware.basicMiddleware(services.server.start)),
];

// const middlewares = utils.framework.createMiddlewares(middlewaresList);

const context = { libs, payload: 'teste' };

utils.framework.middlewareChain(middlewaresList, libs.middlewareChain, context);

const serverMiddlewares = [libs.express.json(), libs.cors(config.cors)];

const app = utils.framework.createServer({
  appName: 'data',
  server: services.server.create({ libs }),
  middlewares: serverMiddlewares,
});

// TODO: 20/06/2023 criar criador de respostas
const resultBuilder = utils.framework.createFunction({
  fn: services.results.createResults,
  validadetion: () => {
    return { status: true };
  },
  results: {
    failure: failure => failure,
    success: success => success,
    error: error => error,
  },
});

// TODO: 20/06/2023 criar respostas
const resultsList = [
  { method: 'exemplo', path: 'exemplo', action: utils.framework.createFunction('getList') },
  { method: 'exemplo', path: 'exemplo', action: utils.framework.createFunction('getOneById') },
  { method: 'exemplo', path: 'exemplo', action: utils.framework.createFunction('getOneByField') },
];
const results2 = resultBuilder(resultsList);

// TODO: 20/06/2023 criar criador de validação
const validationBuilder = utils.framework.createFunction({
  fn: services.validator.createValidator,
  validator: {
    validade: () => {
      return { status: true };
    },
  },
  results: {
    failure: failure => failure,
    success: success => success,
    error: error => error,
  },
});

// TODO: 20/06/2023 criar validações
const validationList = [
  { method: 'exemplo', path: 'exemplo', action: utils.framework.createFunction('getList') },
  { method: 'exemplo', path: 'exemplo', action: utils.framework.createFunction('getOneById') },
  { method: 'exemplo', path: 'exemplo', action: utils.framework.createFunction('getOneByField') },
];
const validations = validationBuilder(validationList);

const { BaseController } = controllers;
const { baseRoutes } = routes;

// Definir o uso das rotas da API
const routesList = [
  { method: 'get', path: '/', action: utils.framework.createFunction('getList') },
  { method: 'get', path: '/:id', action: utils.framework.createFunction('getOneById') },
  { method: 'get', path: '/email/:email', action: utils.framework.createFunction('getOneByField') },
  { method: 'post', path: '/', action: utils.framework.createFunction('createOne') },
  { method: 'put', path: '/:id', action: utils.framework.createFunction('updateById') },
  { method: 'delete', path: '/:id', action: utils.framework.createFunction('deleteById') },
];

const appRoutes = baseRoutes.newRoutes(routesList);
app.use('/', appRoutes);

utils.framework.startServer({ app, config, logger });
